package lib1

import "fmt"

type Lib1Type struct {
	Float  float64
	String string
}

func Func() {
	fmt.Println("Func v5")
}

func Func2() {
	fmt.Println("Func2 v5")
}
